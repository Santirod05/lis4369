import functions as f

def main ():
    f.get_requirements()
    # f.user_input()
    user_input = f.get_user_input()

    # tuple unpacking: tuple values unpacked into variable names! 
    fat, carb, protein = user_input

    # pass user entered values 
    f.calculate_calories(fat, carb, protein) 

if __name__ == "__main__":
   main()