"""Defines three functions:

1. get_requirements()
2. calculate_payroll()
"""


def get_requirements():
    """Accepts 0 args. Displays program requirements."""
    print("Developer: Rafael Rodriguez")
    print("rain_detector")
    print("\nProgram Requirements:\n"
        + "1. Capture user-entered input.\n"
        + "2. Based upon day of week, determine if rain will occur.\n"
        + "3. Note: Program does case-sensitive.\n"

def whatday():
    print("Input: ")
        # get day of week
        day_of_week = float(input("Enter full name of day of week : ")) 

def calculate_rain():
        # constants to represent number of hours worked, holiday hours, and hourly pay rate.
        base_hours = 40
        ot_rate = 1.5
        holiday_rate = 2.0

        # IPO: input > process > output
        # get user data
        print("Input: ")
        # get hours worked and hourly pay rate
        hours = float(input("Enter hours worked : ")) 
        holiday_hours = float(input("Enter holiday hours: "))
        pay_rate = float(input("Enter hourly pay rate: "))

        # process:
        # calculations 
        base_pay = hours * pay_rate
        overtime_hours = hours - base_hours

        # calculate and display gross pay
        if hours > base_hours: 
            # calculate gross pay with overtime

            # calculate overtime pay
            overtime_pay = overtime_hours * pay_rate * ot_rate

            # calculate holiday pay
            holiday_pay = holiday_hours * pay_rate * holiday_rate

            #calculate gross pay 
            gross_pay = base_hours * pay_rate + overtime_pay + holiday_pay
            print_pay(base_pay, overtime_pay, holiday_pay, gross_pay)
        else: 
            # calculate gross pay without overtime, but include holiday pay
            overtime_pay = 0
            holiday_pay = holiday_hours * pay_rate * holiday_rate
            gross_pay = hours * pay_rate + holiday_pay 

            # display pay
            print_pay(base_pay, overtime_pay, holiday_pay, gross_pay)

def print_pay(base_pay, overtime_pay, holiday_pay, gross_pay):
        print("\nOutput: ")
        print("{0:<10} {1:,.2f}" .format("Base: ", base_pay))
        print("{0:<10} {1:,.2f}" .format("Overtime: ", overtime_pay))
        print("{0:<10} {1:,.2f}" .format("Holiday: ", holiday_pay))
        print("{0:<10} {1:,.2f}" .format("Gross: ", gross_pay))