> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions.

## Rafael Rodriguez

### Assingment 4 Requirements:

*Deliverables:*
*Five Parts:*

1. Run demo.py
2. If errors, more than likely missing installations.
3. Test Python Package installer: pip freeze
4. Create at least three functions that are called by the program:
    - main(): calls at least two other functions.
    - get_requirements(): displays the program requirements.
    - data_analysis_2(): displays the following data.

#### README.md file should include the following items:
 
- Assignment requirements.
- Screenshot of data_analysis2 program running.
- Screenshots of Skillsets 10-12:
    - SS10 - Using Dictionaries
    - SS11 - Random Number Generator
    - SS12 - Temperature Conversion Program
- Screenshots of data_analysis2 Jupyter notebook (p1.ipynb)
    
#### Assignment Screenshot and Links:

#### Screenshot of data_analysis2 program running:

![Screenshot of data_analysis2 program running](img/Data_analysis1.png "data_analysis2 screenshot 1")
![Screenshot of data_analysis2 program running](img/Data_analysis2.png "data_analysis2 screenshot 1")
![Screenshot of data_analysis2 program running](img/Data_analysis3.png "data_analysis2 screenshot 1")
![Screenshot of data_analysis2 program running](img/Data_analysis4.png "data_analysis2 screenshot 1")
![Screenshot of data_analysis2 program running](img/Data_analysis5.png "data_analysis2 screenshot 1")
![Screenshot of data_analysis2 program running](img/Data_analysis6.png "data_analysis2 screenshot 1")
![Screenshot of data_analysis2 program running](img/Data_analysis7.png "data_analysis2 screenshot 1")


#### Skillset #10:

![Skillset #10 Screenshot](img/ss10.png "Skillset #10")

#### Skillset #11:

![Skillset #11 Screenshot](img/ss11.png "Skillset #11")

#### Skillset #12:

![Skillset #12 Screenshot](img/ss12.png "Skillset #12")

#### Project1.ipynb:

![Project1.ipynb Screenshot](img/jupyterNotebook1.png "Project1 Jupyter Notebook part 1")
![Project1.ipynb Screenshot](img/jupyterNotebook2.png "Project1 Jupyter Notebook part 2")
![Project1.ipynb Screenshot](img/jupyterNotebook3.png "Project1 Jupyter Notebook part 3")
![Project1.ipynb Screenshot](img/jupyterNotebook4.png "Project1 Jupyter Notebook part 3")