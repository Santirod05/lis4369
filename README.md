> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

#  LIS4369 - Extensible Enterprise Solutions. 

## Rafael Rodriguez

### Assignment Requirements:

*Assignment Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R studio
    - Install Visual Studio Code
    - Create a1_tip_calculator application
    - Create a1 tip calculator Jupyter Notebook
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Backward-engineer (using Python) the payroll_calculator program with overtime
    - Backward-engineer (using Python) the payroll_calculator program without overtime
    - Provide jupyter notebook of payroll_calculator program
    - Skillsets 1-3
        - SS1 - Square Feet to Acres
        - SS2 - Miles Per Gallon
        - SS3 - IT/ICT Student Percentage
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Backward-engineer (using python) the painting_estimator program
    - Exhibit Python iteration structures (loops), function calls
    - Provide Jupyter Notebook of painting_estimator program
    - Skill Sets 4-6
        - SS4 - Calorie Percentage
        - SS5 - Python Selection Structures
        - SS6 - Python Loops
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Run demo.py
    - If errors, more than likely missing installations.
    - Test Python Package installer: pip freeze
    - Create at least three functions that are called by the program:
        -  main(): calls at least two other functions.
        - get_requirements(): displays the program requirements.
        - data_analysis_2(): displays the following data.
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Set up and install R Studio
    - Run lis4369_a5.R and learn_to_use_r.R
    - Display graphs and output
    - Skill sets 13-15
    
### Project Requirements:

*Project Links:*

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Code and run demo.py 
        -  Use the demo.py program to backward-engineer project 1.
    - Update conda, and install necessary Python packages.

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Backward engineer dataresults given from P2 Requirements
    - Run P2 R code to produce values
    - Display graphs and output
