"""Defines three functions:

1. get_requirements()
2. get_user_input()
3. calculate_calories()
"""
#!/usr/bin/env python3
#Developer: Rafael Rodriguez
#Course LIS4369


def get_requirements():
    print("\nDeveloper: Rafael Rodriguez")
    print("\nCalorie Percentage")
    print("\nProgram Requirements:\n"
        + "1. Find calories per gram of fat, carbs, and protein.\n"
        + "2. Calculate Percentages."
        + "3. Must use float data types.\n"
        + "4. Format, right-align numbers and round to two decimal places.\n")

def get_user_input():
        grams_carbs = 0.0
        grams_protein = 0.0
        grams_fat = 0.0

        # get number of calories
        print("Input: ")
        grams_fat = float(input("Enter total fat grams: "))
        grams_carbs = float(input("Enter total carb grams: ")) 
        grams_protein = float(input("Enter total protein grams: "))
        
        return grams_fat, grams_protein, grams_carbs

def calculate_calories(grams_f, grams_p, grams_c):
        total_calories = 0.0 
        percent_fat = 0.0 
        percent_protein = 0.0
        percent_carbs = 0.0

        # process: 
        # calculate total number of calories

        calories_from_fat = grams_f * 9 
        calories_from_protein = grams_p * 4
        calories_from_carbs = grams_c * 4
        total_calories = calories_from_carbs + calories_from_fat + calories_from_protein

        # calculate Percentages 

        percent_fat = calories_from_fat / total_calories
        percent_protein = calories_from_protein / total_calories
        percent_carbs = calories_from_carbs / total_calories

        # print total number of students, percentage of IT students, and Percentage of ICT students

        print("\nOutput: ")
        print("{0:8} {1:>10} {1:>13}" .format("Type", "Calories", "Percentage"))
        print("{0:8} {1:10,.2f} {2:13.2%}".format("Fat", calories_from_fat, percent_fat))
        print("{0:8} {1:10,.2f} {2:13.2%}".format("Carbs", calories_from_carbs, percent_carbs))
        print("{0:8} {1:10,.2f} {2:13.2%}".format("Protein", calories_from_protein, percent_protein))

        
        """ 
        other formatting

        print("{0:17} {1:>5.2f}" .format("Type: ", "Calories", "Percentage"))
        print("{0:17} {1:>5.2f}" .format("Fat: ", total_fat))
        print("{0:17} {1:>5.2%}" .format("Protein: ", total_protein))
        print("{0:17} {1:>5.2%}" .format("Carbs: ", total_carbs))
        """