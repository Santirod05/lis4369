"""Defines two functions:

1. get_requirements()
2. calculate_student_percentage()
"""


def get_requirements():
    """Accepts 0 args. Displays program requirements."""
    print("Developer: Rafael Rodriguez")
    print("IT/ICT Student Percentage")
    print("\nProgram Requirements:\n"
        + "1. Find number of IT/ICT Students in class\n"
        + "2. Calculate IT/ICT Student Percentage.\n"
        + "3. Must use float data type (to Facilitate right-alignment).\n"
        + "4. Format, right-align numbers and round conversion to two decimal places.\n")

def calculate_student_percentage():
        """Accepts 0 args. Calculates IT/ICT student percentage."""
        # variables hold number of IT/ICT students in class
        it_students = 0.0
        ict_students = 0.0
        total = 0.0
        it_percent = 0.0 
        ict_percent = 0.0

        # get number of IT/ICT students
        print("Input: ")
        it_students = int(input("Enter number of IT students: ")) 

        ict_students = int(input("Enter number of ICT students: "))

        # calculate total number of students, percentage of IT students, and Percentage of ICT students 
        total = it_students + ict_students
        it_percent = it_students / total
        ict_percent = ict_students / total

        # print total number of students, percentage of IT students, and Percentage of ICT students

        print("\nOutput: ")
        print("{0:17} {1:>5.2f}" .format("Total Students: ", total))
        print("{0:17} {1:>5.2%}" .format("IT Students: ", it_percent))
        print("{0:17} {1:>5.2%}" .format("ICT Students: ", ict_percent))