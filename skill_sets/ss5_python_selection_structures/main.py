#!/usr/bin/env python3
#Developer: Rafael Rodriguez
#Course: LIS4369

import functions as f

def main():
    f.get_requirements()
    f.get_user_input()

if __name__ == "__main__":
    main()

