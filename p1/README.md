> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions.

## Rafael Rodriguez

### Project 1 Requirements:

*Deliverables:*
*Five Parts:*

1. Code and run demo.py 
2. Use the demo.py program to backward-engineer project 1.
3. Update conda, and install necessary Python packages.
4. Create jupyter notebook for p1 code. 
5. Provide required screenshots.

#### README.md file should include the following items:
 
- Assignment requirements.
- Screenshot of project one program running.
- Screenshots of Skillsets 7-9:
    - SS7 - Using Lists
    - SS8 - Using Tuples
    - SS9 - Using Sets
- Screenshots of project1 Jupyter notebook (p1.ipynb)
    
#### Assignment Screenshot and Links:

#### Screenshot of project one program running:

![Screenshot of project one program running](img/demo_py.png "p1 screenshot")


#### Skillset #7:

![Skillset #7 Screenshot](img/ss7.png "Skillset #7")

#### Skillset #8:

![Skillset #8 Screenshot](img/ss8.png "Skillset #8")

#### Skillset #9:

![Skillset #9 Screenshot](img/ss9.png "Skillset #9")

#### Project1.ipynb:

![Project1.ipynb Screenshot](img/p1_jupyter_notebook1.png "Project1 Jupyter Notebook part 1")
![Project1.ipynb Screenshot](img/p1_jupyter_notebook2.png "Project1 Jupyter Notebook part 2")
![Project1.ipynb Screenshot](img/p1_jupyter_notebook3.png "Project1 Jupyter Notebook part 3")