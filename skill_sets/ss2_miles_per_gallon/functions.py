"""Defines two functions:

1. get_requirements()
2. calculate_miles_per_gallon()
"""


def get_requirements():
    """Accepts 0 args. Displays program requirements."""
    print("Developer: Rafael Rodriguez")
    print("Miles Per Gallon")
    print("\nProgram Requirements:\n"
        + "1. Convert MPG.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversion to two decimal places.\n")

def calculate_miles_per_gallon():
        """Accepts 0 args. Calculates miles driven divided by gallons used, and prints output."""
        # variables hold miles driven by the vehicle and gallons used
        miles_driven = 0.0
        gallons_used = 0.0

        # get square feet in tract
        print("Input: ")
        miles_driven = float(input("Enter miles driven: ")) 

        gallons_used = float(input("Enter gallons of fuel used: "))

        # calculate miles per gallon 
        mpg = miles_driven / gallons_used

        # print Miles per gallon

        print("\nOutput: ")
        print("{0:,.2f} {1} {2:,.2f} {3} {4:,.2f} {5}" .format(miles_driven, "miles driven and =", gallons_used, "gallons used =", mpg, "mpg"))