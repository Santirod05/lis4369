> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions.

## Rafael Rodriguez

### Project 2 Requirements:

*Deliverables:*
*Three Parts:*

1. Backward engineer data results given from P2 Requirements
2. Run R Code to produce vaules
3. Display graphs and output

#### README.md file should include the following items:
 
- Assignment requirements.
- Screenshot of P2 program running on R Studio.
- Screenshot of graphs and output of P2 code.
- Link to Ouput file 

#### Assignment Screenshot and Links:

#### Screenshot of P2 program running on R Studio:

![Screenshot of P2 program running on R Studio](img/lis4369_p2.png "Scrrenshot of P2 program running on R Studio")

#### Screenshot of graphs and output of P2 code:
 
![Project 2 chart 1 Screenshot](img/lis4369_p2_chart1.png "chart 1 Screenshot")
![Project 2 chart 2 Screenshot](img/lis4369_p2_chart2.png "chart 2 Screenshot")

#### Link to Output file:

*Output file:*
[Project 2 Output file Link](rsr_lis4369_p2_output.txt "Project 2 output")
