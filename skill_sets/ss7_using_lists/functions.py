#!/bin/env python3

def get_requirements():
    print("Python Lists")
    print("\nProgram Requirements:\n"
        + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
        + "2. Lists are mutable/changeable-- that is, can insert, update, delete.\n"
        + '3. Create lists - using square brackets [list]: my_list = ["cherries", "apples", "bananas". or anges"].\n'
        + "4. Create a pogram that mirrors the following IPO (input/process/output) format.\n"
        + "Note: user enters number of requested list elements, dynamically rendered below (that is, number of elements can change each run).\n")

# IPO: Input > Process > Output

def user_input():
    # initialize variables and list
    num = 0

    # input: get user data
    print("Input:")
    num = int(input("Enter number of list elements: "))
    return num

print() # print blank line 

def using_lists(num):
    # process
    my_list = [] # create empty list

    # range(stop)
    # stop: NUmber of integers (whole numbers) to generate,
    # starting from zero; e.g., range(3) == [0,1,2]

    for i in range(num): # run loop num times (o to num)
        # prompt user for list element
        my_element = input('Please enter list element' + str(i+1) + ": ")
        my_list.append(my_element)  # append each element to end of list

# Output:

    print("\nOutput: ")
    print("Print my_list: ")
    print(my_list)

    elem = input("\nPlease enter list element: ")
    pos = int(input("Please enter list *index* position (Note: must convert to int): "))

    print("\nCount number of elements in list: ")
    # also works for strings, tuples, dict objects 
    print(len(my_list))

    print("\nSort elements in list alphabetically: ")
    my_list.sort()  # sort Alphabetically 
    print(my_list)

    print("\nReverse list: ")
    my_list.pop()   # delete last element (LIFO)
    print(my_list)

    print("\nDelete second element from list by *index* (note: 1=2nd element): ")
    # pops element at index specified
    # my_list.pop(1)
    # or..
    del my_list[1]
    print(my_list)

    print("\nDelete element from list by *value* (cherries): ")
    my_list.remove('cherries')
    print(my_list)

    print("\nDelete all elements from list: ")
    my_list.clear()
    print(my_list)

    # or..
    # print("\Delete all elements from the list (using slicing): ")
    # del my_list[:]  # delete all elements using slicing
    # print("my_list")   