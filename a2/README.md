> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions.

## Rafael Rodriguez

### Assignment #2 Requirements:

*Deliverables:*
*Four Parts:*

1. Backward-engineer (using Python) the payroll_calculator program with overtime and without.
2. Backward-engineer skillsets #1-3 and Provide screenshots.
3. Test your program using both IDLE and Visual Studio Code.
4. Provide jupyter notebook of payroll_calculator program.

#### README.md file should include the following items:

* Screenshot of payroll_calculator program without overtime
* Screenshot of payroll_calculator program with overtime
* Screenshot of skillsets #1-3
* Jupyter notebook of payroll_calculator

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

#### Screenshot of payroll_calculator program without overtime:

![Screenshot of payroll_calculator no OT](img/no_overtime.png "payroll_calculator no overtime")

#### Screenshot of payroll_calculator program with overtime:

![Screenshot of payroll_calculator with OT](img/overtime.png "payroll_calculator with overtime")

#### Skillset #1:

![Skillset #1 Screenshot](img/ss1.png "Skillset #1")

#### Skillset #2:

![Skillset #2 Screenshot](img/ss2.png "Skillset #2")

#### Skillset #3:

![Skillset #3](img/ss3.png "Skillset #3")

#### a2_payroll.ipynb:

![a2_payroll.ipynb Screenshot](img/a2_jupyter_notebook1.png "A2 Jupyter Notebook part 1")
![a2_payroll.ipynb Screenshot](img/a2_jupyter_notebook2.png "A2 Jupyter Notebook part 2")