#!/bin/env python3

# Pandas = "Python Data Analysis Library"
# Be sure to: pip install pandas-datareader
import datetime as dt
import pandas_datareader as pdr # remote data access for pandas
import matplotlib.pyplot as plt
from matplotlib import style

def get_requirements():
    print("Developer: Rafael S. Rodriguez")
    print("Program Requirements: ")
    print("1. Run demo.py.")
    print("2. If errors, more than likely missing Installations.")
    print("3. Test python package Isntaller: pip freeze")
    print("4. Research how to do the following Installations: ")
    print("\ta. pandas (only if missing)")
    print("\tb. pandas-datareader (only if missing)")
    print("\tc. matplotlib (only if missing)")
    print("5. Create at least three functions that are called by the program: ")
    print("\ta. main(): calls at least two other functions.")
    print("\tb. get_requirements(): displays the program requirements.")
    print("\tc. data_analysis_1(): displays the following data.")

def data_analysis_1():
    start = dt.datetime(2010, 1, 1)
    end = dt.datetime.now() # current date and time
    # end = dt.datatime(2018,10,15)
    # for "end": *must* use Python function for current day/time
    # Federal Reserve Economic Data (FRED): https://fred.stlouisfed.org/
    # Categories: https://fred.stlouisfed.org/categories
    # GDP = Gross domestic product (https://fred.stlouisfed.org/series/GDP)
    # DJIA = Dow jones Industrial average (https://fred.stlouisfed.org/series/DJIA)
    # SP500 = S&P 500 (https://fred.stlouisfed.org/series/SP500)

    # Read data into Pandas DataFrame
    # single series
    # df = pdr.DataReader("GDP", "fred", start, end)

    # multiple series
    df = pdr.DataReader(["DJIA", "SP500"], "fred", start, end)

    print("\nPrint number of records: ")
    index = df.index
    number_of_rows = len(index)
    print(number_of_rows)
    #print(len(df.index))

    # why is it important to run the following print statement...
    print(df.columns)

    print("\nPrint data frame: ")
    print(df)   #   Note: for Efficiency, only prints 60--not *all* records

    print("\nPrint first five lines:")
    #   Note: "Date" is lower than the other columns as it is treated as an index 
    df1 = df.head()
    print(df1)

    print("\nPrint last five lines:")
    df2 = df.tail()
    print(df2)

    print("\nPrint first 2 lines: ")
    df3 = df.head(2)
    print(df3)

    print("\nPrint last 2 lines: ")
    df4 = df.tail(2)
    print(df4)

    style.use('ggplot')

    df['DJIA'].plot()
    df['SP500'].plot()
    plt.legend()
    plt.show()

get_requirements()
data_analysis_1()