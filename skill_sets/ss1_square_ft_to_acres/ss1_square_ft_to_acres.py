#!/usr/bin/env python3
# Developer: Rafael Rodriguez

print("Square Feet To Acres")
print("Program Requirements:\n"
        + "1. Research: Number of square feet to acre of land.\n"
        + "2. Must use float daata type for user input and calculation.\n"
        + "3. Format and round conversion to two decimal places.\n")

print("Input: ")
ft = float(input("Enter square feet: "))

# Feet to Acre
acre = ft / 43560

print("\nOutput: ")
print(f'{ft:,.2f} square feet = {acre:.2f} acres')