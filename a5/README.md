> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions.

## Rafael Rodriguez

### Assingment 5 Requirements:

*Deliverables:*
*Four Parts:*

1. Set up and install R Studio
2. Run learn_to_use_r.R program
3. Run lis4369_a5.R program
4. code and run skill sets 13-15

#### README.md file should include the following items:
 
- Assignment requirements.
- Screenshot of learn_to_use_r.R program running on R Studio.
- Screenshot of lis4369_a5.R program running on R Studio.
- Provide 2 graphs from each program 
- Screenshots of Skillsets 13-15:
    - SS13 - Sphere Volume Calculator
    - SS14 - Calculator With Error Handling
    - SS15 - File Write Read
    
#### Assignment Screenshot and Links:

#### Screenshot of learn_to_use_r.R program running:

![Screenshot of learn_to_use_r.R program running](img/learn_to_use_r.png "learn_to_use_r.R program running on R Studio screenshot")

#### Graphs from learn_to_use_r.R program:
 
![learn_to_use_r_chart1 Screenshot](img/learn_to_use_r_chart1.png "learn_to_use_r_ chart #1")
![learn_to_use_r_chart2 Screenshot](img/learn_to_use_r_chart2.png "learn_to_use_r chart # 2")

#### Screenshot of lis4369_a5.R program running:

![Screenshot of lis4369_a5.R program running](img/lis4369_a5.png "lis4369_a5.R program running on R Studio screenshot")

#### Graphs from lis4369_a5.R program:

![lis4369_a5_chart1 Screenshot](img/lis4369_a5_chart1.png "lis4369_a5.R chart #1")
![lis4369_a5_chart2 Screenshot](img/lis4369_a5_chart2.png "lis4369_a5.R chart #2")

#### Skillset #13:

![Skillset #13 Screenshot](img/ss13.png "Skillset #13")

#### Skillset #14:

![Skillset #14 Screenshot](img/ss14.png "Skillset #14")

#### Skillset #15:

![Skillset #15 Screenshot](img/ss15.png "Skillset #15")
