> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions.

## Rafael Rodriguez

### Assignment 3 Requirements:

*Deliverables:*
*Six Parts:*

1. Calculate home interior paint cost (w/o primer)
2. Must use float data types
3. Must use SQFT_PER_GALLON constant of 350
4. Must use iteration structure (i.e., loop)
5. Format, right-align, and round numbers to two decimal places
6. Create at least five functions that are called by the program:
    - main(): calls two other functions: get_requirements() and estimate_painting_cost()
    - get_requirements(): displays the program requirements
    - estimate_painting_cost(): calculates interior home painting and calls print functions
    - print_painting_estimate(): displays painting costs
    - print_painting_percentage(): displays painting cost percentages

#### README.md file should include the following items:

- Screenshot of A3 ERD that links to the image:
    - Assignment requirements
    - Screenshots of a3_painting_estimator application running
    - Screenshots of Skillsets 4-6:
        - SS4 - Calorie Percentage
        - SS5 - Python Selection Structures
        - SS6 - Python Loops
    - Screenshots of a3_painting_estimator Jupyter notebook (a3_painting_estimator.ipynb)
    
#### Assignment Screenshot and Links:

#### Screenshot of a3_painting_estimator application:

![Screenshot of a3_painting_estimator application](img/a3.png "Painting estimator")


#### Skillset #4:

![Skillset #4 Screenshot](img/ss4.png "Skillset #4")

#### Skillset #5:

![Skillset #5 Screenshot](img/ss5.png "Skillset #5")

#### Skillset #6:

![Skillset #6 Screenshot](img/ss6.png "Skillset #6")

#### a3_painting_estimator.ipynb:

![a3_painting_estimator.ipynb Screenshot](img/a3_jupyter_notebook1.png "A3 Jupyter Notebook part 1")
![a3_painting_estimator.ipynb Screenshot](img/a3_jupyter_notebook2.png "A3 Jupyter Notebook part 2")